<?php

//	HTML Header
$html =  <<<EOT
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" href="https://picnicss.com/style.min.css">

	<script type="module" src="https://unpkg.com/x-frame-bypass"></script>

	<title>On This Day In Twistory</title>
  </head>
  <body>
EOT;

//	GETs come in like twistory/edent
$params = explode("/", $_SERVER["HTTP_HOST"].explode('?', $_SERVER["REQUEST_URI"], 2)[0]);
$GLOBALS["params"] = $params;

//	Get Screen_name
if( isset($params[1]) )  { $screen_name = htmlspecialchars($params[1]); } else { $screen_name = null;}

//	Show the history
if (null != $screen_name){
	//	Set up Twitter Connection
	require_once ('codebird.php');
	$cb = \Codebird\Codebird::getInstance();
	\Codebird\Codebird::setBearerToken('');

	//	Get details from Twitter API
	$user_data = (array) $cb->users_show("screen_name={$screen_name}", true);

	//	Error
	if (isset($user_data["errors"])) {
		$error = htmlspecialchars($user_data["httpstatus"]);
		header("Location: /?error={$error}",TRUE,302);
		die();
	}

	$created_at = $user_data["created_at"];
	$user_first_year = substr($created_at, -4);
	$this_year = date("Y");
	$year_range = $this_year - $user_first_year;

	//	Headings
	$html .= "<table><tr>";

	for ($i=$year_range; $i >= 1; $i--) {
		$today	= new DateTime("today"   );
		$interval = 'P'.$i.'Y';
		$year	= ($today->sub(   new DateInterval($interval)))->format("Y");

		$html .= "<th>{$year}</th>";
	}
	$html .= "</tr>";
	$html .= "<tr>";

	//	OTD Tweet iFrames
	for ($i=$year_range; $i >= 1; $i--) {
		$today	= new DateTime("today"   );
		$tomorrow = new DateTime("tomorrow");

		$interval = 'P'.$i.'Y';
		$todayMinusYear	= ($today->sub(   new DateInterval($interval)))->format("Y-m-d");
		$tomorrowMinusYear = ($tomorrow->sub(new DateInterval($interval)))->format("Y-m-d");

		$html .= <<<EOT
		<td>
		<iframe is="x-frame-bypass"
		src="https://mobile.twitter.com/search?q=from%3A{$screen_name}%20(until%3A{$tomorrowMinusYear}%20since%3A{$todayMinusYear})&src=typed_query&f=live"
		width="256" height="3072"></iframe>
		</td>
	EOT;
	}
	$html .= "</tr></table>";
} else {
	$form  = <<< EOT
<main>
	<header style="text-align:center;margin-bottom:-1em;margin-top:1em;">
		<h1>On This Day in Twistory</h1>
	</header>
	<div id="content" class="visual flex one two-800">

	<div class="content">
		<h1>What were you Tweeting about this time last year?</h1>
		<p>This site shows you "On This Day" Tweets.
		<p>Stick your Twitter username in the box, and see what you were saying on this day in previous years.
		<p>This is a "best effort" service with no guarantee.
		<p>Doesn't work on iPhone.
	</div>

	<div class="card">
		<section>
			<form action="/redirect.php" method="GET" target="_self">
				<div>
					<h2><label for="screen_name">Twitter Screen Name @:</label></h2><br>
					<input type="text" id="screen_name" name="screen_name" placeholder="@edent">
				</div>
				<input type="submit" value="Let's see their Twistory">
			</form>
			<br>
			<p>We do not collect <em>any</em> of your personal data.
			<p>This website will not ask you to log in.
			<p>None of your personal data is sent to Twitter by us.
			<p>If a Twitter user has changed their screen name, results may be inconsistent.
			<p>Created by <a href="https://shkspr.mobi/blog/">Terence Eden</a>.
			<p><a href="https://gitlab.com/edent/on-this-day-twitter">Open Source Code available on GitLab</a>.
		</section>
	</div>
</main>
EOT;
	$html .= $form;
}

$html .= "</body></html>";
echo $html;
die();