<?php
$screen_name = $_GET["screen_name"];
$screen_name = trim($screen_name);
$screen_name = str_replace("@","",$screen_name);

require_once ('codebird.php');
$cb = \Codebird\Codebird::getInstance();
\Codebird\Codebird::setBearerToken('');

$user_data = (array) $cb->users_show("screen_name={$screen_name}", true);

if (isset($user_data["errors"])) {
	$error = htmlspecialchars($user_data["httpstatus"]);
	header("Location: /?error={$error}",TRUE,302);
	die();
}

// 302 Found
header("Location: /{$screen_name}",TRUE,302);
die();